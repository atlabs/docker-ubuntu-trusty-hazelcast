#!/bin/bash
set -e

if [ "$1" = 'hazelcast' ]; then

	: ${HZ_HOST="$(hostname --ip-address)"}

	: ${HZ_PORT="5701"}

	: ${HZ_GROUP="dev"}
	: ${HZ_PASSWORD="dev-pass"}

	: ${HZ_HEAP="none"}
	if [ "${HZ_HEAP}" = "none" ]; then
        HZ_HEAP=`/calculate-heap-limit.sh`
	fi

	MEM=""
	if [ "${HZ_HEAP}" != "default" ]; then
        MEM="-Xms${HZ_HEAP} -Xmx${HZ_HEAP}"
    fi

	echo "HZ_HOST: ${HZ_HOST}"
	echo "HZ_PORT: ${HZ_PORT}"
    echo "HZ_HEAP: ${HZ_HEAP}"

    cd ${HZ_HOME}
	exec gosu hazelcast java -cp "${HZ_HOME}/*" ${MEM} \
	  -Dhazelcast.host=${HZ_HOST} \
	  -Dhazelcast.port=${HZ_PORT} \
	  -Dhazelcast.group=${HZ_GROUP} \
	  -Dhazelcast.password=${HZ_PASSWORD} \
	  -Dhazelcast.config=${HZ_HOME}/hazelcast.xml \
	  com.hazelcast.core.server.StartServer

fi

exec "$@"
