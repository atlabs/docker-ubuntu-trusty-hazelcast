# docker-ubuntu-trusty-hazelcast
A docker build to run Hazelcast on Marathon.


# Building and Pushing

Create this image as follows:

	sudo docker build -t atlp/ubuntu-trusty-hazelcast:3.7.5 3.7.5/.

Now that our image is built, push it to your registry:

	sudo docker push atlp/ubuntu-trusty-hazelcast:3.7.5


# Environment Variables

## HZ_HOST

Sets the host IP number to which to bind:

	$ docker run -it --net host -e HZ_HOST=10.10.25.14 atlp/ubuntu-trusty-hazelcast:3.7.5
	 
If no `HZ_HOST` is set, the IP number associated with the hostname will be used.	 

## HZ_PORT

The port to use, by default `5701`.

## HZ_GROUP & HZ_PASSWORD

The property `HZ_GROUP` sets the group name for the Hazelcast cluster. By default this is set to `dev`. The
property `HZ_PASSWORD` sets the passwords for this group. This is by default set to `dev-pass`. 

You are encouraged to change these two properties to suit your needs.

Once set, you can then configure your client to connect to the cluster as follows:
 
	ClientConfig clientConfig = new ClientConfig();
	clientConfig.getGroupConfig().setName("dev").setPassword("dev-pass");
	clientConfig.getNetworkConfig().addAddress("10.10.25.14:5701"); 

## HZ_HEAP

The heap size used for Kafka. Default is 0.75 of available memory for the container. If
you set this manually, make sure to not exceed with a reasonable margin the container 
memory!  






